package cn.tannn.tregistry.server.constant;

import cn.tannn.tregistry.core.subscribe.MessageEvent;
import org.springframework.http.codec.ServerSentEvent;
import reactor.core.publisher.Flux;

/**
 * sse
 *
 * @author <a href="https://tannn.cn/">tnnn</a>
 * @version V1.0
 * @date 2024/4/26 下午11:54
 */
public class SseConstant {

    public static Flux<ServerSentEvent<MessageEvent>> notificationFlux = Flux.empty();

}
