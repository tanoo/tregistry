package cn.tannn.tregistry.server;

import cn.tannn.tregistry.server.constant.SseConstant;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class TregistryApplication {

    public static void main(String[] args) {
        SpringApplication.run(TregistryApplication.class, args);
    }


    @PreDestroy
    public void destory() {
        log.info("系统关闭，清理资源……");
        SseConstant.notificationFlux.doOnComplete(() -> {
            log.info("system stopped , dispose notificationFlux ");
        }).subscribe();
    }
}
