package cn.tannn.tregistry.client;

/**
 * 接口常量
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2024/4/28 8:55
 */
public class HttpPathConstant {

    public static final String REG_PATH = "/reg?service=";
    public static final String UNREG_PATH = "/unreg?service=";
    public static final String RENEW_PATH = "/renew?service=";
    public static final String RENEWS_PATH = "/renews?services=";

    public static final String FINDALL_PATH = "/findAll?service=";
    public static final String VERSION_PATH = "/version?service=";

    public static final String INFO_PATH = "/info";
    public static final String CLUSTER_PATH = "/cluster";
    public static final String LEADER_PATH = "/leader";
    public static final String SUBSCRIBE_PATH = "/subscribe?service=";
    public static final String SNAPSHOT_PATH = "/snapshot";

}
