package cn.tannn.tregistry.core.subscribe;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author tnnn
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageEvent {
    private String service;
    private Long version;

    public MessageEvent(Map<String, Long> VERSIONS) {
        VERSIONS.forEach((k,v) -> {
            this.service = k;
            this.version = v;
        });

    }
}
